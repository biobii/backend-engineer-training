@extends('layouts.master')

@section('title', 'Create Post')

@section('content')
    <div class="container">
        <h1 class="mt-5">Create Post</h1>
        <hr>

        <form action="{{ route('posts.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label>Title</label>
                <input type="text" name="title" class="form-control" placeholder="The title of the post">
            </div>
            <div class="form-group">
                <label>Category</label>
                <select name="category_id" class="form-control">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Body</label>
                <textarea rows="5" type="text" name="body" class="form-control" placeholder="The body of the post"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Publish</button>
        </form>
    </div>
@endsection