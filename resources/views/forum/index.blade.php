@extends('layouts.master')

@section('title', 'Forum')

@section('content')
    <div class="container">
        <h1 class="mt-5">Simple Forum</h1>
        <p class="lead mb-1">Hello, John Doe</p>
        <a href="{{ route('posts.create') }}" class="btn btn-sm btn-primary mb-3">
            Create new post
        </a>
        <hr>

        <div class="row post">
            @foreach ($posts as $post)
                <div class="col-md-12 mb-2">
                    <a href="{{ route('posts.show', $post->slug) }}" class="h4">
                        {{ $post->title }}
                    </a>
                    <p class="text-muted mb-2">{{ $post->user->name }}</p>
                    <p class="text-muted mt-2">{{ $post->comments->count() }} comments</p>
                    <hr>
                </div>
            @endforeach
            
            <center style="margin:0 auto">
                {!! $posts->links() !!}
            </center>
        </div>
    </div>
@endsection