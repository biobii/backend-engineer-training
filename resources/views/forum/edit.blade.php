@extends('layouts.master')

@section('title', 'Edit Post')

@section('content')
    <div class="container">
        <h1 class="mt-5">Edit Post</h1>
        <hr>

        <form action="{{ route('posts.update', $post->slug) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label>Title</label>
                <input type="text" name="title" class="form-control"
                    placeholder="The title of the post" value="{{ old('title') ?? $post->title }}">
            </div>
            <div class="form-group">
                <label>Category</label>
                <select name="category_id" class="form-control">

                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}"
                            {{ ($category->id === $post->category_id) ? 'selected="true"' : '' }}">
                            {{ $category->name }}
                        </option>
                    @endforeach

                </select>
            </div>
            <div class="form-group">
                <label>Body</label>
                <textarea rows="5" type="text" name="body" class="form-control"
                    placeholder="The body of the post">{{ old('body') ?? $post->body }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection