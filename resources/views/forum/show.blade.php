@extends('layouts.master')

@section('title', $post->title)

@section('css')
    <style>
        #delete-form, #delete-comment {
            display: inline-block;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <h2 class="mt-5">{{ $post->title }}</h2>

        @can ('update', $post)
            <a href="{{ route('posts.edit', $post->slug) }}"class="btn btn-sm btn-warning">
                Edit
            </a>
        @endcan

        @can ('delete', $post)
            <form id="delete-form" onsubmit="return confirm('Are you sure?')"
                action="{{ route('posts.destroy', $post->slug) }}" method="POST">
                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
            </form>
        @endcan

        <p class="h5 mb-2 mt-4">{{ $post->body }}</p>
        
        <a href="#" class="badge badge-secondary">{{ $post->category->name }}</a>
        <p>{{ $post->created_at }}</p>
        <hr>
        <div class="row post">
            <h5 class="ml-4">{{ $post->comments->count() }} comments</h5>
            <hr>
            @foreach ($post->comments as $comment)
                <div class="col-md-12 mb-2">
                    <p class="mb-1">
                        <strong>{{ $comment->user->name }}</strong> -
                        <small>{{ $comment->updated_at }}</small>
                    </p>
                    <p class="mt-1 mb-1">{{ $comment->comment }}</p>

                    @can ('update', $comment)
                        <a href="{{ route('comments.edit', $comment->id) }}" class="btn btn-sm btn-warning">Edit</a>
                    @endcan

                    @can ('delete', $comment)
                        <form id="delete-comment" onsubmit="return confirm('Are you sure?')"
                            action="{{ route('comments.destroy', $comment->id) }}" method="POST">
                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>
                    @endcan
                    <hr>
                </div>
            @endforeach

            @auth
                <div class="col-md-12">
                    <form action="{{ route('comments.store', $post->slug) }}" method="POST">
                        @csrf
                        <textarea rows="5" type="text" name="comment"
                            class="form-control mb-2" placeholder="Your comment.."></textarea>
                        <button type="submit" class="btn btn-primary mb-5">Submit</button>
                    </form>
                </div>
            @endauth
        </div>
    </div>
@endsection