<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .category a {
            display: inline-block;
            padding: 6px;
            margin: 10px 2px;
            font-size: 14px;
        }
        .post div a {
            display: inline-block;
        }
        .container {
            width: auto;
            max-width: 680px;
            padding: 0 15px;
        }
        .footer {
            background-color: #f5f5f5;
        }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
      }
    </style>
    @yield('css')
  </head>

    <body class="d-flex flex-column h-100">
        <main role="main">
            @yield('content')
        </main>

        <footer class="footer mt-auto py-3">
            <div class="container">
                <span class="text-muted">&copy; 2019 - Backend Engineer</span>
                <div class="float-right">
                    @foreach ($categories as $category)
                        <a href="#" class="ml-4">{{ $category->name }}</a>
                    @endforeach
                </div>
            </div>
        </footer>

        @yield('js')
    </body>
</html>
