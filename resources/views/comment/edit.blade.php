@extends('layouts.master')

@section('title', 'Edit Comment')

@section('content')
    <div class="container">
        <h1 class="mt-5">Edit Comment</h1>
        <hr>

        <form action="{{ route('comments.update', $comment->id) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label>Comment</label>
                <textarea rows="5" type="text" name="comment" class="form-control"
                    placeholder="Your comment">{{ old('comment') ?? $comment->comment }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection