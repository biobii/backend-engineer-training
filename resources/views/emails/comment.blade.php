@component('mail::message')
    Hello, {{ $post->user->name }}<br>
    Komentar baru pada postingan <strong>{{ $post->title }}</strong>

@component('mail::button', ['url' => route('posts.show', $post->slug), 'color' => 'primary'])
    Lihat Post
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent