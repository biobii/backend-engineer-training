<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);

        factory(App\Models\User::class, 5)->create();
        factory(App\Models\Post::class, 5)->create();
        factory(App\Models\Comment::class, 5)->create();
    }
}
