<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Frontend',
            'description' => 'Topik Frontend'
        ]);

        Category::create([
            'name' => 'Backend',
            'description' => 'Topik Backend'
        ]);
        
        Category::create([
            'name' => 'Mobile',
            'description' => 'Topik Mobile'
        ]);
    }
}
