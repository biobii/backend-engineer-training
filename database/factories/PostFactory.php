<?php

use App\Models\User;
use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(App\Models\Post::class, function (Faker $faker) {
    return [
        'user_id' => User::inRandomOrder()->first()->id,
        'category_id' => Category::inRandomOrder()->first()->id,
        'title' => $faker->sentence,
        'body' => $faker->paragraph(6)
    ];
});
