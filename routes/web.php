<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('posts', 'PostController');

Route::post('comments/{slug}/store', 'CommentController@store')->name('comments.store');
Route::get('comments/{id}/edit', 'CommentController@edit')->name('comments.edit');
Route::put('comments/{id}/update', 'CommentController@update')->name('comments.update');
Route::delete('comments/{id}', 'CommentController@destroy')->name('comments.destroy');

Auth::routes();
