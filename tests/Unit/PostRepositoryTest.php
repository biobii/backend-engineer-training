<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Post;
use App\Repositories\PostRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->post = new PostRepository(new Post);
        $this->data = [
            'title' => 'awesome title',
            'user_id' => 1,
            'category_id' => 1,
            'body' => 'here the body of the post'
        ];
    }

    /** @test */
    public function createPost()
    {
        $post = $this->post->create($this->data);

        $this->assertInstanceOf(Post::class, $post);
        $this->assertEquals(
            ucwords($this->data['title']),
            $post->title
        );
    }

    /** @test */
    public function allPost()
    {
        $posts = $this->post->findAll();

        $this->assertInstanceOf(
            \Illuminate\Database\Eloquent\Collection::class,
            $posts
        );
    }

    /** @test */
    public function pagingPost()
    {
        $posts = $this->post->paginate();

        $this->assertInstanceOf(
            \Illuminate\Pagination\LengthAwarePaginator::class,
            $posts
        );
    }

    /** @test */
    public function updatePost()
    {
        $post = $this->post->update(
            Post::find(1)->slug,
            $this->data
        );

        $this->assertEquals(1, $post);
    }

    /** @test */
    public function deletePost()
    {
        $post = $this->post->delete(
            Post::find(1)->slug,
            $this->data
        );

        $this->assertEquals(1, $post);
    }
}
