## Setup Project

1. Clone project and run composer update.
2. Copy `env.example` to `.env`.
3. `php artisan key:generate`.
4. `php artisan migrate --seed`.
5. `php artisan schedule:run`.
6. `php artisan serve`.
