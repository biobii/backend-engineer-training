<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Cache;
use App\Repositories\Contracts\CategoryRepository;

class CategoriesComposer
{
    /**
     * The category repository implementation.
     *
     * @var CategoryRepository
     */
    protected $category;

    /**
     * Create a new categories composer.
     *
     * @param  CategoryRepository  $category
     * @return void
     */
    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $minutes = 60 * 24;
        $categories = Cache::remember('categories', $minutes, function () {
            return $this->category->findAll(['id', 'name']);
        });

        $view->with('categories', $categories);
    }
}