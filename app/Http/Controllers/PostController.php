<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests\PostRequest;
use App\Repositories\Contracts\PostRepository;
use App\Repositories\Contracts\CategoryRepository;

class PostController extends Controller
{
    /**
     * @var PostRepository
     */
    protected $post;
    
    /**
     * Constructor.
     *
     * @param PostRepository $post
     * @return void
     */
    public function __construct(PostRepository $post)
    {
        $this->middleware('auth')
            ->except(['index', 'show']);

        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('forum.index', [
            'posts' => $this->post->paginate(
                5,
                ['id', 'user_id', 'title', 'slug'],
                ['user:id,name', 'comments:id,post_id']
            )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CategoryRepository $category
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryRepository $category)
    {
        return view('forum.create', [
            'categories' => $category->findAll(['id', 'name'])
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $request->request->add(['user_id' => Auth::user()->id]);
        $post = $this->post->create($request->all());

        return redirect()->route('posts.show', $post->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = $this->post->findBySlug(
            $slug,
            ['*'],
            ['user', 'category', 'comments.user']
        );

        return view('forum.show', [
            'post' => $post
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param CategoryRepository $category
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryRepository $category, $slug)
    {
        $post = $this->post->findBySlug($slug, ['*'], ['category']);
        $this->authorize('update', $post);

        return view('forum.edit', [
            'post' => $post,
            'categories' => $category->findAll(['id', 'name'])
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PostRequest $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $slug)
    {
        $post = $this->post->findBySlug($slug);
        $this->authorize('update', $post);

        $this->post->update($slug, $request->all());

        return redirect()->route('posts.show', $slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $this->authorize('delete', $this->post->findBySlug($slug));
        $this->post->delete($slug);

        return redirect()->route('posts.index');
    }
}
