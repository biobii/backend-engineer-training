<?php

namespace App\Http\Controllers;

use Auth;
use App\Events\CommentEvent;
use App\Http\Requests\CommentRequest;
use App\Repositories\Contracts\PostRepository;
use App\Repositories\Contracts\CommentRepository;

class CommentController extends Controller
{
    /**
     * @var CommentRepository
     */
    protected $comment;

    /**
     * Constructor.
     * 
     * @param CommentRepository $comment
     * @return void
     */
    public function __construct(CommentRepository $comment)
    {
        $this->middleware('auth');

        $this->comment = $comment;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CommentRequest  $request
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request, PostRepository $post, $slug)
    {
        $post = $post->findBySlug($slug);
        $comment = $post->comments()->create([
                'user_id' => Auth::user()->id,
                'comment' => $request->comment
            ]);

        event(new CommentEvent($comment));

        return redirect()->route('posts.show', $slug);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('comment.edit', [
            'comment' => $this->comment->find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CommentRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, $id)
    {
        $this->comment->update($id, $request->all());
        $comment = $this->comment->find($id, ['id', 'post_id'], ['post:id,slug']);

        return redirect()->route('posts.show', $comment->post->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slug = $this->comment->find($id)->post->slug;
        $this->comment->delete($id);

        return redirect()->route('posts.show', $slug);
    }
}
