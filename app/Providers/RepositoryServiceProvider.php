<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\PostRepository',
            'App\Repositories\PostRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\CategoryRepository',
            'App\Repositories\CategoryRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\CommentRepository',
            'App\Repositories\CommentRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
