<?php

namespace App\Mail;

use App\Models\Post;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewCommentNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Post
     */
    protected $post;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('obi@example.dev', 'Mohammad Robih')
                    ->markdown('emails.comment')
                    ->with('post', $this->post);
    }
}
