<?php

namespace App\Listeners;

use Mail;
use App\Events\CommentEvent;
use App\Mail\NewCommentNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailCommentNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentEvent $event
     * @return void
     */
    public function handle(CommentEvent $event)
    {
        $post = $event->comment->post;
        $postAuthor = $event->comment->post->user;

        Mail::to($postAuthor->email)->send(new NewCommentNotification($post));
    }
}
