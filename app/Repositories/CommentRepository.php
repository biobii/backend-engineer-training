<?php

namespace App\Repositories;

use App\Models\Comment;
use App\Repositories\Contracts\CommentRepository as CommentInterface;

class CommentRepository extends BaseRepository implements CommentInterface
{
    /**
     * Model
     *
     * @var 
     */
    protected $model;

    /**
     * Constructor.
     * 
     * @param \App\Models\Comment $comment
     * @return void
     */
    public function __construct(Comment $comment)
    {
        $this->model = $comment;
    }
}