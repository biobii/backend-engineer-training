<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\Contracts\CategoryRepository as CategoryInterface;

class CategoryRepository extends BaseRepository implements CategoryInterface
{
    /**
     * Model
     *
     * @var 
     */
    protected $model;

    /**
     * Constructor.
     * 
     * @param \App\Models\Category $category
     * @return void
     */
    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    /**
     * Get category by name.
     * 
     * @param string $name
     * @param array $columns
     * @param array $relations
     * @return \App\Models\Category
     */
    public function findByName($name, $columns = ['*'], $relations = [])
    {
        return $this->model
            ->with($relations)
            ->select($columns)
            ->where('name', $name)
            ->first();
    }
}