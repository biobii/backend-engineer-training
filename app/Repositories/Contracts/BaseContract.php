<?php

namespace App\Repositories\Contracts;

interface BaseContract
{
    /**
     * Get resource by id.
     *
     * @param int $id
     * @param array $columns
     * @param array $relations
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($id, $columns = ['*'], $relations = []);

    /**
     * Get all resources.
     *
     * @param array $columns
     * @param array $relations
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll($columns = ['*'], $relations = []);

    /**
     * Get resources with paging.
     *
     * @param int $perPage
     * @param array $columns
     * @param array $relations
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 10, $columns = ['*'], $relations = []);

    /**
     * Create new resource.
     *
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($attributes);

    /**
     * Update existing resource by id.
     *
     * @param int $id
     * @param array $attributes
     * @return int
     */
    public function update($id, $attributes);

    /**
     * Delete existing resource by id.
     *
     * @param int $id
     * @return mixed
     */
    public function delete($id);
}