<?php

namespace App\Repositories\Contracts;

use App\Repositories\Contracts\BaseContract;

interface CategoryRepository extends BaseContract
{
    /**
     * Get category by name.
     * 
     * @param string $name
     * @param array $columns
     * @param array $relations
     * @return \App\Models\Category
     */
    public function findByName($name, $columns = ['*'], $relations = []);
}