<?php

namespace App\Repositories\Contracts;

use App\Repositories\Contracts\BaseContract;

interface PostRepository extends BaseContract
{
    /**
     * Get post by slug.
     * 
     * @param string $slug
     * @param array $columns
     * @param array $relations
     * @return \App\Models\Post
     */
    public function findBySlug($slug, $columns = ['*'], $relations = []);

    /**
     * Update existing resource by slug.
     *
     * @param string $slug
     * @param array $attributes
     * @return int
     */
    public function update($slug, $attributes);

    /**
     * Delete existing resource by slug.
     *
     * @param string $slug
     * @return mixed
     */
    public function delete($slug);
}