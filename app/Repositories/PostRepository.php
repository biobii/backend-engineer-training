<?php

namespace App\Repositories;

use App\Models\Post;
use App\Repositories\Contracts\PostRepository as PostInterface;

class PostRepository extends BaseRepository implements PostInterface
{
    /**
     * Model
     *
     * @var 
     */
    protected $model;

    /**
     * Constructor.
     * 
     * @param \App\Models\Post $post
     * @return void
     */
    public function __construct(Post $post)
    {
        $this->model = $post;
    }

    /**
     * Get post by slug.
     * 
     * @param string $slug
     * @param array $columns
     * @param array $relations
     * @return \App\Models\Post
     */
    public function findBySlug($slug, $columns = ['*'], $relations = [])
    {
        return $this->model
            ->with($relations)
            ->select($columns)
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Update existing resource by slug.
     *
     * @param string $slug
     * @param array $attributes
     * @return int
     */
    public function update($slug, $attributes)
    {
        return $this->model
            ->where('slug', $slug)
            ->first()
            ->update($attributes);
    }

    /**
     * Delete existing resource by slug.
     *
     * @param string $slug
     * @return mixed
     */
    public function delete($slug)
    {
        return $this->model
            ->where('slug', $slug)
            ->first()
            ->delete();
    }
}