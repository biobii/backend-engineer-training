<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Contracts\BaseContract;

abstract class BaseRepository implements BaseContract
{
    /**
     * Model
     * 
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Constructor.
     * 
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get resource by id.
     *
     * @param int $id
     * @param array $columns
     * @param array $relations
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($id, $columns = ['*'], $relations = [])
    {
        return $this->model->with($relations)->select($columns)->find($id);
    }

    /**
     * Get all resources.
     *
     * @param array $columns
     * @param array $relations
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll($columns = ['*'], $relations = [])
    {
        return $this->model->with($relations)->select($columns)->get();
    }

    /**
     * Get resources with paging.
     *
     * @param int $perPage
     * @param array $columns
     * @param array $relations
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 10, $columns = ['*'], $relations = [])
    {
        return $this->model->with($relations)->select($columns)->paginate($perPage);
    }

    /**
     * Create new resource.
     *
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Update existing resource by id.
     *
     * @param int $id
     * @param array $attributes
     * @return int
     */
    public function update($id, $attributes)
    {
        return $this->model->find($id)->update($attributes);
    }

    /**
     * Delete existing resource by id.
     *
     * @param int $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }
}